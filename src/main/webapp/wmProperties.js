var _WM_APP_PROPERTIES = {
  "activeTheme" : "bricks",
  "defaultLanguage" : "en",
  "displayName" : "MySwisher",
  "homePage" : "Main",
  "name" : "MySwisher",
  "platformType" : "MOBILE",
  "supportedLanguages" : "en",
  "type" : "APPLICATION",
  "version" : "1.0"
};